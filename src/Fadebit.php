<?php
class Fadebit{
    function __construct($App){
        $this->App = $App;
        $this->Portals = [
            'Domaining' => 'https://domaining.fadebit.com/',
            'Cardini' => 'https://cardini.fadebit.com/',
            'Payer' => 'https://payer.fadebit.com/'
        ];
    }

    function App(){
        return $this->App;
    }

    function Portals(){
        return $this->Portals;
    }
}